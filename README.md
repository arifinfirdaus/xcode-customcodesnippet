# README - Xcode-CustomSnippet #

This is some custom code snippet to make development faster for Ice House's standard coding style.

![](Demo.gif)

## Getting Started 

### Prerequisites

Xcode

### Installing

Import this files (not the folder) to :
```
~/Library/Developer/Xcode/UserData/CodeSnippets/
```
Use Finder's `cmd+Shift+Go`.

## How to Use

try to type 
```
mark
```
or other supported snippet mentioned above, and you will see the code snippet.

## Supported Code Snippet

### Comments
```
mark, fixme, todo
```

### MARK
```
markhelpers, markpublicproperties, markprivateproperties, markinitialization, markpublicmethods, markprivatemethods
```

### import
- Import RxSwift
```
imrxswift
```

### RxSwift
- RxSwift DisposeBag Declaration
```
disposebag
```

## Entity / Model Layer
### Request Parameter
- Ice House's Request Parameter (Data Transfer Object)
```
ihrequestparameter
```

## Domain / Core Layer
### Use Case 
- Ice House's Style Use Case (RxSwift)
```
ihusecaseimpl
```

## Presentation Layer
### ViewModel
- Ice House's ViewModel Layer (iOS)
```
ihviewmodel
```

### ViewController
- Ice House's ViewModel Constructor Injection for UIViewController (iOS)
```
ihinjectviewmodel
```

### UITableView
```
uitableviewdatasource, uitableviewdelegate
```

## Authors

* **Arifin Firdaus** - *Initial work on GitHub, modified to Ice House's Need* - [Xcode-CustomCodeSnippet](https://github.com/arifinfrds/Xcode-CustomCodeSnippet)
